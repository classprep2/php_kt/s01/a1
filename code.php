<?php
function getFullAddress($country, $city, $province, $specificAddress){
	return "$specificAddress, $city, $province, $country";
}

function getLetterGrade($grade){

	if($grade >= 98 && $grade < 101){
		$letter = "A+";
	}
	else if($grade >= 95 && $grade <= 97){
		$letter = "A";
	}
	else if($grade >= 92 && $grade <= 94){
		$letter = "A-";
	}

	else if($grade >= 89 && $grade <= 91){
		$letter = "B+";
	}
	else if($grade >= 86 && $grade <= 88){
		$letter = "B";
	}
	else if($grade >= 83 && $grade <= 85){
		$letter = "B-";
	}

	else if($grade >= 80 && $grade <= 82){
		$letter = "C+";
	}
	else if($grade >= 77 && $grade <= 79){
		$letter = "C";
	}
	else if($grade >= 75 && $grade <= 76){
		$letter = "C-";
	}

	else if($grade < 75){
		$letter = "F";
	}


	return "$grade is equaivalent to $letter";
}
?>